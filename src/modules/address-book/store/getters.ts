import AddressBookState from '../types/AddressBookState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<AddressBookState, any> = {
  getCustomer: (state) => state.customer
}
