import Attributes from './Attributes';

export default interface Address {
  city: string,
  country: string,
  firstName: string,
  house: string,
  lastName: string,
  phone: string,
  postcode: string,
  region: string,
  street: string,
  block: string,
  floor: string,
  avenue: string,
  custom_attributes: Attributes[],
  default_shipping: boolean,
  default_billing: boolean
}
