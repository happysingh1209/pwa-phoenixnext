import FreeGiftState from '../types/FreeGiftState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<FreeGiftState, any> = {
  getFreeGiftProducts: (state) => state.freegifts
}
