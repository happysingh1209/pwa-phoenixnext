import { nostoModule } from './store'
import { StorefrontModule } from '@vue-storefront/core/lib/modules'
export const KEY = 'nosto'

const nostoSnippet = (nostoAppId) => (function (w, d, u, h, a) {
  h = d.getElementsByTagName('head')[0];
  a = d.createElement('script');
  a.async = 1;
  a.src = u
  a.onload = function () {
    w.nostojs = w.nostojs || (cb => { (w.nostojs.q = w.nostojs.q || []).push(cb); });
  }
  h.appendChild(a);
})(window as any, document, '//connect.nosto.com/include/' + nostoAppId);

export const NostoModule: StorefrontModule = function ({ store, router, appConfig }) {
  if (typeof window !== 'undefined') {
    nostoSnippet(appConfig.aureatelabs.nosto.appId);
  }
  store.registerModule(KEY, nostoModule)
}
