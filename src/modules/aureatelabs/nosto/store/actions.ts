import Vue from 'vue'
import { ActionTree } from 'vuex';
import NostoState from '../types/NostoState'
import NostoRecommendationState from '../types/NostoRecommendationState'
import * as types from './mutation-types';
import { prepareQuery } from '@vue-storefront/core/modules/catalog/queries/common';
declare const window: any;
const actions: ActionTree<NostoState, any> = {
  list (context, recommendationIds: NostoRecommendationState) {
    let prepareArray = {};
    if (typeof window !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .viewFrontPage()
          .setPlacements(recommendationIds)
          .load()
          .then(response => {
            for (const [key, value] of Object.entries(response.recommendations)) {
              if (value['result_type'] === 'REAL') {
                var newArray = [];
                for (const [divId, divValue] of Object.entries(value['products'])) {
                  let pid = parseInt(value['products'][divId].product_id);
                  if (!isNaN(pid) && pid > 0) {
                    newArray.push(pid);
                  }
                }
                prepareArray[key] = newArray
              }
            }
            context.commit(types.FETCH_ALL_RECOMMEND_PRODUCTS, prepareArray);
            context.dispatch('getProductList', prepareArray)
            return prepareArray
          }).catch(error => {
            console.error('There was an error!', error);
          });
      });
    }
  },
  async getProductList ({ state, rootState, dispatch, commit }, productIdsList) {
    // if (rootState.route.query.nostodebug) {
    try {
      let resultArray = {};
      for (const [divId, productIds] of Object.entries(productIdsList)) {
        let newProductsQuery = prepareQuery({ filters: [{ key: 'id', value: { 'in': productIds } }] });
        const newProductsResult = await dispatch('product/list', { query: newProductsQuery, skipCache: true },
          {
            root: true
          }).then(res => {
          return res.items;
        }).catch(error => {
          console.error('There was an error!', error);
        });
        resultArray[divId] = newProductsResult
      }
      commit(types.FETCH_ALL_RECOMMENDATIONS, resultArray);
    } catch (error) {
      console.log(error)
    }
    // }
  },
  sendOrderData (context, nostoData) {
    if (typeof window !== 'undefined') {
      window.nostojs(api => {
        api.defaultSession()
          .addOrder(nostoData)
          .setPlacements(['order-related'])
          .load()
          .then(data => {
            console.log('Data Recommendations: ' + data.recommendations);
          }).catch(error => {
            console.error('There was an error!', error);
          })
      })
    }
  }
}

export default actions
