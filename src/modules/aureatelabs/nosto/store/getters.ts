
import NostoState from '../types/NostoState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<NostoState, any> = {
  getRecommendationList: (state) => {
    return state.recommendations
  },
  getProductIds: (state) => {
    return state.ids
  }
}
