import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.REWARD_FETCH_REWARD] (state, rewardPoints) {
    state.rewardPoints = rewardPoints || []
  }
}
