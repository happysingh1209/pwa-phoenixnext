import Vue from 'vue'
import { ActionTree } from 'vuex';
import PaymentState from '../types/PaymentState'
import rootStore from '@vue-storefront/core/store'
import i18n from '@vue-storefront/i18n'
import Payment from '../types/Payment';
var crypto = require('crypto');

const actions: ActionTree<PaymentState, any> = {
  processPayment ({ dispatch }, PaymentData: Payment) {
    function formatPrice (price) {
      var my_string = '' + Number(price).toFixed(2).replace('.', '');
      while (my_string.length < 12) {
        my_string = '0' + my_string;
      }
      return my_string;
    }
    var merchantID = '764764000004349';
    var secretKey = 'D5327FACFC1AADF7DBCC7CF725367950B12807B54354E5A407C574C75492063E'
    var desc = 'Phoenix Order Payment';
    var uniqueTransactionCode = Date.now();
    var currencyCode = '764';
    var amt = '000000001099';
    // var amt = formatPrice(rootStore.getters['cart/getTotals'][Object.keys(rootStore.getters['cart/getTotals']).length - 1].value);
    var panCountry = 'TH';
    // Customer Information
    var cardholderName = rootStore.getters['checkout/getPersonalDetails'].firstName + ' ' + rootStore.getters['checkout/getPersonalDetails'].lastName;
    var version = '9.9';

    var xml = '<PaymentRequest>'
    xml += '<merchantID>' + merchantID + '</merchantID>';
    xml += '<uniqueTransactionCode>' + uniqueTransactionCode + '</uniqueTransactionCode>';
    xml += '<desc>' + desc + '</desc>';
    xml += '<amt>' + amt + '</amt>';
    xml += '<currencyCode>' + currencyCode + '</currencyCode>';
    xml += '<panCountry>' + panCountry + '</panCountry>';
    xml += '<cardholderName>' + cardholderName + '</cardholderName>';
    xml += '<encCardData>' + PaymentData.encryptedCardInfo + '</encCardData>';
    xml += '</PaymentRequest>';
    var paymentPayload = window.btoa(xml);

    var signature = crypto.createHmac('sha256', secretKey).update(paymentPayload).digest('hex').toUpperCase();
    var payloadXML = '<PaymentRequest>';
    payloadXML += '<version>' + version + '</version>';
    payloadXML += '<payload>' + paymentPayload + '</payload>';
    payloadXML += '<signature>' + signature + '</signature>';
    payloadXML += '</PaymentRequest>';
    var payload = window.btoa(payloadXML);
    dispatch('submitPayment', payload)
  },
  processQRPayment ({ dispatch }) {
    function formatPrice (price) {
      var my_string = '' + Number(price).toFixed(2).replace('.', '');
      while (my_string.length < 12) {
        my_string = '0' + my_string;
      }
      return my_string;
    }
    var merchantID = '764764000004349';
    var secretKey = 'D5327FACFC1AADF7DBCC7CF725367950B12807B54354E5A407C574C75492063E'

    // Transaction Information
    var desc = 'Phoenix Order Payment';
    var uniqueTransactionCode = Date.now();
    var currencyCode = '764';
    var amt = '000000001099';
    // var amt = formatPrice(rootStore.getters['cart/getTotals'][Object.keys(rootStore.getters['cart/getTotals']).length - 1].value);
    var panCountry = 'TH'; // Not required for APM payment

    // Customer Information
    var cardholderName = rootStore.getters['checkout/getPersonalDetails'].firstName + ' ' + rootStore.getters['checkout/getPersonalDetails'].lastName; // Not required for APM payment

    /** NOT REQUIRED FOR APM PAYMENT
    //Encrypted card data
    $encCardData = $_POST['encryptedCardInfo'];

    //Retrieve card information for merchant use if needed
    $maskedCardNo = $_POST['maskedCardInfo'];
    $expMonth = $_POST['expMonthCardInfo'];
    $expYear = $_POST['expYearCardInfo'];
    **/

    // Payment Options
    var paymentChannel = '123'; // Set transaction as Alternative Payment Method
    var agentCode = 'AXS'; // APM agent code
    var channelCode = 'KIOSK'; // APM channel code
    let date = new Date();
    var paymentExpiry = `${date.getFullYear()}-${(date.getMonth() + 1)}-${date.getDate()} 23:59:59`; // pay slip expiry date (optional). format yyyy-MM-dd HH:mm:ss
    var mobileNo = '81238888';// customer mobile number
    var cardholderEmail = rootStore.getters['checkout/getPersonalDetails'].emailAddress; // customer email address

    // Request Information
    var version = '9.9';

    // Construct payment request message
    var xml = '<PaymentRequest>';
    xml += '<merchantID>' + merchantID + '</merchantID>';
    xml += '<uniqueTransactionCode>' + uniqueTransactionCode + '</uniqueTransactionCode>';
    xml += '<desc>' + desc + '</desc>';
    xml += '<amt>' + amt + '</amt>';
    xml += '<currencyCode>' + currencyCode + '</currencyCode>';
    xml += '<panCountry>' + panCountry + '</panCountry>';
    xml += '<cardholderName></cardholderName>';
    xml += '<paymentChannel>' + paymentChannel + '</paymentChannel>';
    xml += '<agentCode>' + agentCode + '</agentCode>';
    xml += '<channelCode>' + channelCode + '</channelCode>';
    xml += '<paymentExpiry>' + paymentExpiry + '</paymentExpiry>';
    xml += '<mobileNo>' + mobileNo + '</mobileNo>';
    xml += '<cardholderEmail>' + cardholderEmail + '</cardholderEmail>';
    xml += '<encCardData></encCardData>';
    xml += '</PaymentRequest>';
    var paymentPayload = window.btoa(xml); // Convert payload to base64

    var signature = crypto.createHmac('sha256', secretKey).update(paymentPayload).digest('hex').toUpperCase();
    var payloadXML = '<PaymentRequest>';
    payloadXML += '<version>' + version + '</version>';
    payloadXML += '<payload>' + paymentPayload + '</payload>';
    payloadXML += '<signature>' + signature + '</signature>';
    payloadXML += '</PaymentRequest>';
    var payload = window.btoa(payloadXML);
    dispatch('submitPayment', payload)
  },
  submitPayment (context, payload) {
    var form = document.createElement('form');
    var element1 = document.createElement('input');

    form.method = 'POST';
    form.action = 'https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/PaymentAuth.aspx';

    element1.value = payload;
    element1.name = 'paymentRequest';
    form.appendChild(element1);

    document.body.appendChild(form);

    form.submit();
  }

}
export default actions
