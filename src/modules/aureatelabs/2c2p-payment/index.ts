import { paymentMethod } from './store'
import { StorefrontModule } from '@vue-storefront/core/lib/modules'

export const KEY = 'p2c2pPayment'
export const PaymentModule: StorefrontModule = function ({ store, router, appConfig }) {
  if (typeof document !== 'undefined') {
    let recaptchaScript = document.createElement('script')
    recaptchaScript.setAttribute('src', `https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/api/my2c2p.1.6.9.min.js`)
    document.head.appendChild(recaptchaScript)
  }

  store.registerModule(KEY, paymentMethod)
}
