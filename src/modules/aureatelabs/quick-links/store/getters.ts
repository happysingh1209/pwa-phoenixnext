
import { GetterTree } from 'vuex';

export const getters: GetterTree<any, any> = {
  getQuickLinksList: (state) => state.quicklinks
}
