export const AL_BRANDS = 'brands'
export const AL_FETCH_BRANDS = AL_BRANDS + '/FETCH_BRANDS'
export const AL_FETCH_BRANDS_BY_SLUG = AL_BRANDS + '/FETCH_BRANDS_BY_SLUG'
