import { Module } from 'vuex'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const BrandsModuleStore: Module<any, any> = {
  namespaced: true,
  state: {
    brands: [],
    brandBySlug: []
  },
  mutations,
  actions,
  getters
}
