import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.AL_FETCH_BRANDS] (state, brands) {
    state.brands = brands || []
  },
  [types.AL_FETCH_BRANDS_BY_SLUG] (state, brands) {
    state.brandBySlug = brands || []
  }
}
