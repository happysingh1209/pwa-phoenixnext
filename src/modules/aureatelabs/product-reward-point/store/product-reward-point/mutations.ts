import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.PRODUCT_REWARD_FETCH_REWARD] (state, productRewardPoints) {
    state.productRewardPoints = productRewardPoints || []
  }
}
